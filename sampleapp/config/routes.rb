Rails.application.routes.draw do
  resources :posts
  root :to => "posts#main"
  # get '/post' => 'post#index'
  # get '/post/new' => 'post#new'
  # post 'post' => 'post#create'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
