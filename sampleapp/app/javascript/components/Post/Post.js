import React, {Component} from "react"
import axios from 'axios';
import { Link } from 'react-router-dom'
import './Post.css'

class Post extends Component{


    state ={
        posts:[]
 
     };

    // constructor(props) {
    //     super(props);
    //     this.state ={
    //        posts:[]
    
    //     };
    //     //this.imagestore = this.imagestore.bind(this);
    //   }
  
      componentDidMount() {
         //const post = new Post();
          onload =() => {
            // image  has been loaded
            this.onLoadHandler();
          };
  
      }


    onLoadHandler(){
        axios.get( 'http://localhost:4100/posts' )
            .then( response => {
               //dispatch(setPosts(response.data));
               console.log(response);
               this.setState({posts:response.data});
               //console.log(this.state.posts);
            } )
            .catch( error => {
                console.log(error);
            } );
    }

    renderAllPosts = () => {
        return(
          <div >
            {this.state.posts.map(post => (

              <div key={post.id} className="Post">
                  <table className="Table">
                      <tr>
                        <td>Id: {post.id}</td>
                        <td>User Name: {post.name}</td>
                        <td>Post: {post.comment}</td>
                        <td>Created at: {post.created_at}</td>
                      </tr>
                      <tr>
                          <td>Edit Delete</td>
                      </tr>
                  </table>
                  </div>
            ))}
          </div>
        )
      }

    
    render(){
        return(
            <div>
                {this.renderAllPosts()}
            </div>
        );
    }
}

export default Post