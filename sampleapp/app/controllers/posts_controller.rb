class PostsController < ApplicationController
  def index
    @posts = Post.all
    render json: @posts.to_json
  end

  def main
    render "main"
  end
  
  def show
    @post = Post.find_by(id: params[:id])
  end

  def new
    @post = Post.new
  end
  
  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_to '/posts'
    else
      render 'new'
    end
  end

  def destroy
    @post = Post.find_by(id: params[:id])
    if @post.delete
      redirect_to '/posts'
    end
  end

  def edit
    @post = Post.find(params[:id])
    #@post.update(name: params[:post][:name], description: params[:post][:comment])
    #redirect_to '/posts'
  end

  def update
    @post = Post.find(params[:id])
    @post.update(name: params[:post][:name], comment: params[:post][:comment])
    redirect_to '/posts'
  end

  private

  def post_params
    params.require(:post).permit(:name, :comment)
  end
end
