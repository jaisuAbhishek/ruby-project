class Reservation
    def initialize
        @pno = Array.new(275)
	    @name = Array.new(275)
	    @phno = Array.new(275)
	    @age = Array.new(275)
	    @cl = Array.new(275)
	    @pcount = 0
	    @pnum = 1
	    @max1 = 75
	    @max2 = 125
	    @max3 = 175
    end

    def menu
        choice=0
        until choice==6
            puts "Please enter your choice \n 1. Book Ticket \n 2. Cancel Ticket \n 3. Search Passenger \n 4. Reservation Chart \n 5. Display Unreserved Tickets \n 6. Exit"
            choice = gets.chomp.to_i
            case choice
            when 1
                doBook()
            when 2
                doCancel()
            when 3
                doSearch()
            when 4
                doDisplayList()
            when 5
                doDisplayUnreserve()
            when 6
                puts "Thank you for using our service.\nPlease visit again"  
            else
                puts "Invalid Choice!!!"
            end
        end
    end

    def doBook
        puts "Please enter the class of ticket"
		puts "1. AC\t 2. First\t 3. Sleeper\t"
		c=gets.chomp.to_i
		puts "Please enter no. of tickets"
		t=gets.chomp.to_i
		ticketAvailable=0
		if c==1 && @max1>=t
			ticketAvailable=1
        elsif c==2 && @max2>=t
			ticketAvailable=1
		elsif c==3 && @max3>=t
            ticketAvailable=1
        end
		if ticketAvailable==1
			for i in 0...t do
				@pno[@pcount]=@pnum
				puts "Please enter your name"
				@name[@pcount]=gets.chomp
				puts "Please enter your age"
				@age[@pcount]=gets.chomp.to_i
				@cl[@pcount]=c
				puts "Please enter your phone no"
				@phno[@pcount]=gets.chomp
				@pcount+=1
				@pnum+=1
				puts "Ticket successfully booked"
            end

			if c==1
				@max1-=t
				puts "Please pay Rs. #{t*1500}"
            elsif(c==2)
				@max2-=t
				puts "Please pay Rs. #{t*1200}"
            elsif(c==3)
				@max3-=t
                puts "Please pay Rs. #{t*1000}"
            end
        else
            puts "Seats are not available"
        end
    end

    def doCancel
        t_pno=Array.new[275]
		t_name=Array.new[275]
		t_phno=Array.new[275]
		t_age=Array.new[275]
		t_cl=Array.new[275]
		t_pcount=0
		passengerFound=0
		puts "Please enter your passenger no."
		p=gets.chomp.to_i
		for i in 0...@pcount do
			if @pno[i]!=p
				t_pno[t_pcount]=@pno[i]
				t_name[t_pcount]=@name[i]
				t_phno[t_pcount]=@phno[i]
				t_age[t_pcount]=@age[i]
				t_cl[t_pcount]=@cl[i]
				t_pcount+=1
			else
				passengerFound=1
				if @cl[i]==1
					@max1+=1
			    	puts "Please collect refund of Rs.1500"
                elsif @cl[i]==2
					@max2+=1
					puts "Please collect refund of Rs.1000"
                elsif @cl[i]==3
					@max3+=1
                    puts "Please collect refund of Rs.500"
				end
            end
		end
		if passengerFound==1
			@pno=t_pno
			@name=t_name
			@age=t_age
			@cl=t_cl
			@phno=t_phno
			@pcount=t_pcount
			puts "ticket successfully cancelled"
        end
    end

    def doSearch
        pFound=0
        puts "Please enter passenger no. to search"
        pasNo=gets.chomp.to_i
        for i in 0...@pcount do
            if @pno[i]==pasNo
                puts "Details Found"
                puts "Name = #{@name[i]}\nClass = #{@cl[i]}\nPhone No. = #{@phno[i]}"
                pFound=1
            end
        end
        if pFound == 0 
            puts "Details Not Found!!"
        end
    end

    def doDisplayList
        puts "Passenger list in AC First class \npno \t name \t\t age \t phno"
        for i in 0...@pcount do
            if @cl[i]==1
                puts "#{@pno[i]}\t#{@name[i]}\t\t#{@age[i]}\t#{@phno[i]}"
            end
        end
        puts "Passenger list in AC Second class \npno \t name \t\t age \t phno"
        for i in 0...@pcount do
            if @cl[i]==2
                puts "#{@pno[i]}\t#{@name[i]}\t\t#{@age[i]}\t#{@phno[i]}"
            end
        end
        puts "Passenger list in Sleeper class \npno \t name \t\t age \t phno"
        for i in 0...@pcount do
            if @cl[i]==3
                puts "#{@pno[i]}\t#{@name[i]}\t\t#{@age[i]}\t#{@phno[i]}"
            end
        end
    end

    def doDisplayUnreserve
        puts "AC First Class - #{@max1} \nAC Second Class - #{@max2} \nSleeper - #{@max3}"
    end
end

ticket_booking = Reservation.new
ticket_booking.menu
