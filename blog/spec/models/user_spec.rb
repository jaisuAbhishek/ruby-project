require 'rails_helper'

RSpec.describe User, type: :model do
  p '------------------------------------------------------------'
  context 'validation tests' do
    it 'ensures email presence' do
      user=User.new(name: 'testcase', mobile_no:'0980798978',password:'B@Db0y199a').save
      expect(user).to eq(false)
    end

    it 'ensures email format' do
      user=User.new(name: 'testcase',email:'test.casom', mobile_no:'0980798978',password:'B@Db0y199a').save
      expect(user).to eq(false)
    end

    it 'ensures mobile_no presence' do
      user=User.new(name: 'testcase',email:'testcase@testcase.com',password:'B@Db0y199a').save
      expect(user).to eq(false)
    end

    it 'ensures mobile_no format' do
      user=User.new(name: 'testcase',email:'testcase@testcase.com', mobile_no:'abhi' ,password:'B@Db0y199a').save
      expect(user).to eq(false)
    end

    it 'ensures password presence' do
      user=User.new(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978').save
      expect(user).to eq(false)
    end

    it 'ensures password format' do
      user=User.new(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B9a').save
      expect(user).to eq(false)
    end

    it 'ensures each coloumns format' do
      user=User.new(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a').save
      expect(user).to eq(true)
    end
  end

  context 'scope tests' do
  end
end
