require 'rails_helper'

RSpec.describe PostsController, type: :controller do
    before(:each) do
    @user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
    session[:user_id] =  @user.id
  end

  context "Success Cases" do
    it "#create method success" do
      post :create, params: {post: "Post Created"}
      expect(response.status).to eq(200)
    end

    it "#update method success" do
      post :create, params: {post: "Post Created"}
      post=JSON.parse(response.body)
      #p "=======#{post["id"]}"
      put :update, params: {id:post["id"],post: "Post Updated"}
      expect(response.status).to eq(200)
    end

    it "#Delete method success" do
      post :create, params: {post: "Post Created"}
      post=JSON.parse(response.body)
      #p "=======#{post["id"]}"
      delete :destroy, params: {id:post["id"]}
      expect(response.status).to eq(200)
    end

     it "#ShowAll method success" do
        post :create, params: {post: "Post Created 1"}
        post :create, params: {post: "Post Created 2"}
        post :create, params: {post: "Post Created 3"}
        get :show_all
       expect(response.status).to eq(200)
     end

     it "#my_post method success" do
        post :create, params: {post: "Post Created 1"}
        post :create, params: {post: "Post Created 2"}
        post :create, params: {post: "Post Created 3"}
        get :mypost
       expect(response.status).to eq(200)
     end

    it "#show single post method success" do
      post :create, params: {post: "Post Created"}
      post=JSON.parse(response.body)
      #p "=======#{post["id"]}"
      get :show, params: {id:post["id"]}
      expect(response.status).to eq(200)
    end
  end

  context 'Failure Cases' do
    it "#create method failure # Post should not be nil or empty" do
      post :create, params: {post: ""}
      expect(response.status).to eq(422)
    end

    it "#create method failure # Session is not present" do
      session[:user_id]=nil
      post :create, params: {post: "Post Created"}
      expect(response.status).to eq(422)
    end

    it "#update method failure # Post should not be nil or empty" do
      post :create, params: {post: "Post Created"}
      post=JSON.parse(response.body)
      #p "=======#{post["id"]}"
      put :update, params: {id:post["id"],post: ""}
      expect(response.status).to eq(422)
    end

    it "#update method failure # Session is not present" do
     
      post :create, params: {post: "Post Created"}
      post=JSON.parse(response.body)
      #p "=======#{post["id"]}"
      session[:user_id]=nil
      put :update, params: {id:post["id"],post: "Post Updated"}
      expect(response.status).to eq(422)
    end

    it "#delete method failure # Post is not present in database" do
      post :create, params: {post: "Post Created"}
      post=JSON.parse(response.body)
      #p "=======#{post["id"]}"
      post = Post.find_by(id: post["id"])
      post.delete
      delete :destroy, params: {id:post["id"]}
      expect(response.status).to eq(422)
    end

    it "#delete method failure # Session is not present" do
      post :create, params: {post: "Post Created"}
      post=JSON.parse(response.body)
      #p "=======#{post["id"]}"
      session[:user_id]=nil
      delete :destroy, params: {id:post["id"]}
      expect(response.status).to eq(422)
    end

    it "#show single post method failure # Post is not present in Database" do
      post :create, params: {post: "Post Created"}
      post=JSON.parse(response.body)
      #p "=======#{post["id"]}"
      post = Post.find_by(id: post["id"])
      post.delete
      get :show, params: {id:post["id"]}
      expect(response.status).to eq(404)
    end

    it "#show single post method failure # Session is not present" do
      post :create, params: {post: "Post Created"}
      post=JSON.parse(response.body)
      #p "=======#{post["id"]}"
      session[:user_id]=nil
      get :show, params: {id:post["id"]}
      expect(response.status).to eq(422)
    end

     it "#ShowAll method failure # Session not present" do
        post :create, params: {post: "Post Created 1"}
        post :create, params: {post: "Post Created 2"}
        post :create, params: {post: "Post Created 3"}
        session[:user_id]=nil
        get :show_all
       expect(response.status).to eq(422)
     end

    

     it "#my_post method failure # Session not present" do
        post :create, params: {post: "Post Created 1"}
        post :create, params: {post: "Post Created 2"}
        post :create, params: {post: "Post Created 3"}
        session[:user_id]=nil
        get :mypost
       expect(response.status).to eq(422)
     end
  end
end
