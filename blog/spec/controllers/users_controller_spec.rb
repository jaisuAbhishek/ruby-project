require 'rails_helper'

RSpec.describe UsersController, type: :controller do
    context 'Main Cases' do
        subject { get :main }

    it "renders the main template" do
      expect(subject).to render_template(:main)
      expect(subject).to render_template("main")
      #expect(subject).to render_template("gadgets/index")
    end

    it "does not render a different template" do
      expect(subject).to_not render_template("index")
    end
    end
    context 'Success Cases' do
        it '#verify success case' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            p "#{user.id}"
            post :verify, params: {email:'testcase@testcase.com',password:'B@Db0y199a'}
            expect(response).to be_success
        end

        it '#create success case' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            expect(response).to be_success
        end

        it '#update success case' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'testccc',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            expect(response).to be_success
        end

        it '#destroy success case' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :destroy
            expect(response).to be_success
        end

        it '#logout success case' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            post :verify, params: {email:'testcase@testcase.com',password:'B@Db0y199a'}
            get :logout
            expect(response).to be_success
        end

        it '#user_details success case' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            post :verify, params: {email:'testcase@testcase.com',password:'B@Db0y199a'}
            get :user_details, id: session[:user_id].to_json
            expect(response).to be_success
        end

        it '#get_login success case' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            post :verify, params: {email:'testcase@testcase.com',password:'B@Db0y199a'}
            get :get_login
            expect(response).to be_success
        end

    end


    context 'Failure Cases' do
        it '#verify Password Wrong failure case' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            p "#{user.id}"
            post :verify, params: {email:'testcase@testcase.com',password:'B@Db0y1'}
            expect(response.status).to eq(401)
        end

        it '#verify Email Wrong failure case' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            p "#{user.id}"
            post :verify, params: {email:'testcase@test.com',password:'B@Db0y199a'}
            expect(response.status).to eq(401)
        end

        it '#create failure case #Email is blank' do
            post :create, params: {name: 'testcase',email:'', mobile_no:'0980798978',password:'B@Db0y199a'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Email format is wrong' do
            post :create, params: {name: 'testcase',email:'abhi', mobile_no:'0980798978',password:'B@Db0y199a'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Email should be unique' do
            post :create, params: {name: 'testcase',email:'test@test.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            post :create, params: {name: 'test',email:'test@test.com', mobile_no:'0930758978',password:'B@Db0o0y199a'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Mobile is blank' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'',password:'B@Db0y199a'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Mobile format is wrong #Should be Number' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'abh',password:'B@Db0y199a'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Mobile format is wrong #Length should be 10' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'11223',password:'B@Db0y199a'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Mobile no should be unique' do
            post :create, params: {name: 'testcase',email:'test@tcom.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            post :create, params: {name: 'test',email:'test@test.com', mobile_no:'0980798978',password:'B@Db0o0y199a'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Password is blank' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:''}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Password format is wrong # Must contain 8 or more characters' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db1'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Password format is wrong # Must contain a digit' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Dboyqwer'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Password format is wrong # Must contain a lower case character' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@DBOY1993'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Password format is wrong # Must contain an upper case character' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'b@dboy1993'}
            expect(response.status).to eq(422)
        end

        it '#create failure case #Password format is wrong # Must contain a symbol' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'BADboy1993'}
            expect(response.status).to eq(422)
        end


        it '#Update failure case #Email is blank' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'testcase',email:'', mobile_no:'0980798978',password:'B@Db0y199a'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Email format is wrong' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'testcase',email:'abhi', mobile_no:'0980798978',password:'B@Db0y199a'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Email should be unique' do
            user1=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798969',password:'B@Db0y199a')
            user=User.create!(name: 'testcase',email:'test@test.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'test',email:'testcase@testcase.com', mobile_no:'0930758978',password:'B@Db0o0y199a'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Mobile is blank' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'testcase',email:'testcase@test.com', mobile_no:'',password:'B@Db0y199a'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Mobile format is wrong #Should be Number' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'testcase',email:'testcase@test.com', mobile_no:'abh',password:'B@Db0y199a'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Mobile format is wrong #Length should be 10' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'testcase',email:'testcase@test.com', mobile_no:'11223',password:'B@Db0y199a'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Mobile no should be unique' do
            user1=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798969',password:'B@Db0y199a')
            user=User.create!(name: 'testcase',email:'testcase@test.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'test',email:'test@testcase.com', mobile_no:'0980798969',password:'B@Db0o0y199a'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Password is blank' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'test',email:'test@test.com', mobile_no:'0980798959',password:''}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Password format is wrong # Must contain 8 or more characters' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'test',email:'test@test.com', mobile_no:'0980798959',password:'B@Db1'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Password format is wrong # Must contain a digit' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'test',email:'test@test.com', mobile_no:'0980798959',password:'B@Dboyqwer'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Password format is wrong # Must contain a lower case character' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'test',email:'test@test.com', mobile_no:'0980798959',password:'B@DBOY1993'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Password format is wrong # Must contain an upper case character' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'test',email:'test@test.com', mobile_no:'0980798959',password:'b@dboy1993'}
            expect(response.status).to eq(422)
        end

        it '#Update failure case #Password format is wrong # Must contain a symbol' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :update, params: {name: 'test',email:'test@test.com', mobile_no:'0980798959',password:'BADboy1993'}
            expect(response.status).to eq(422)
        end

        it '#destroy failure case # Session Deleted' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            session[:user_id] =  user.id
            post :destroy
            begin
               @result= User.find(user.id)
            rescue => exception
                @result=nil
            end
            expect(@result).to be_nil
        end

        it '#destroy failure case #Session was not set' do
            user=User.create!(name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a')
            post :destroy
            expect(response.status).to eq(404)
        end

        it '#logout failure case' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            post :verify, params: {email:'testcase@testcase.com',password:'B@Db0y199a'}
            get :logout
            expect(session).to be_empty
        end

        it '#user_details failure case' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            post :verify, params: {email:'testcase@testcase.com',password:'B@Db0y199a'}
            user=User.find(session[:user_id])
            user.destroy!
            get :user_details, id: session[:user_id].to_json
            expect(response.status).to eq(404)
        end

        it '#get_login failure case # User is not present' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            post :verify, params: {email:'testcase@testcase.com',password:'B@Db0y199a'}
            user=User.find(session[:user_id])
            user.destroy!
            get :get_login
            expect(response.status).to eq(404)
        end

        it '#get_login failure case # Session was not set' do
            post :create, params: {name: 'testcase',email:'testcase@testcase.com', mobile_no:'0980798978',password:'B@Db0y199a'}
            get :get_login
            expect(response.status).to eq(404)
        end

    end

    context 'Post #Create' do
        
    end
end
