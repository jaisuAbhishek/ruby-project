import React, { Component, Fragment } from "react";
import axios from "axios";

import authContext from "../../../context/auth-context";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  withRouter,
  Redirect
} from "react-router-dom";
import Button from "../../../UI/Button/Button";

import "./FullPost.css";

class FullPost extends Component {
  state = {
    post: [],
    user: []
  };

  componentDidMount() {
    console.log(this.props.match.params.id);
    axios
      .get("http://localhost:3300/posts/" + this.props.match.params.id)
      .then(response => {
        console.log(response.data);
        let postr = JSON.parse(response.data.post);
        let userr = JSON.parse(response.data.user);
        console.log(postr);
        // const updatedPost = post.map(post => {
        //   return {
        //     ...post
        //   };
        // });
        this.setState({ post: postr, user: userr });
        console.log(this.state.post);
        console.log(this.state.user);
      })
      .catch(error => {
        console.log(error);
      });
  }

  deletePostHandler = () => {
    axios
      .delete("http://localhost:3300/posts/" + this.props.match.params.id)
      .then(response => {
        console.log(response);
      });
  };

  render() {
    let admin = null;
    console.log(this.props.match.params.userId, "  ", this.state.post.user_id);
    if (this.props.match.params.userId == this.state.post.user_id) {
      admin = (
        <div>
          <authContext.Consumer>
            {context => {
              return (
                <Fragment>
                  <Button clicked={this.deletePostHandler}>Delete Post</Button>|
                  <Link
                    to={{
                      pathname:
                        "/" +
                        this.state.post.user_id +
                        "/EditPost/" +
                        this.state.post.id,
                      state: {
                        id: this.state.post.id,
                        postData: this.state.post.post
                      }
                    }}
                  >
                    Edit Post
                  </Link>
                  |
                </Fragment>
              );
            }}
          </authContext.Consumer>
        </div>
      );
    }

    let post = <p style={{ textAlign: "center" }}>Please select a Post!</p>;
    if (this.props.match.params.id) {
      post = <p style={{ textAlign: "center" }}>Loading...!</p>;
    }
    if (this.state.post) {
      post = (
        <div className="FullPost">
          <h1>{this.state.post.id}</h1>
          <p>{this.state.post.post}</p>
          <h5>{this.state.post.created_at}</h5>
          <Link to={"/User/" + this.state.user.id}>
            <h5>{this.state.user.name}</h5>
          </Link>
          {admin}
        </div>
      );
    }
    return <div>{post}</div>;
  }
}
FullPost.contextType = authContext;
export default withRouter(FullPost);
