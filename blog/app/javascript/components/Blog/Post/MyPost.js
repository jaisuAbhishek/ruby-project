import React, { Component, Fragment } from "react";
import Button from "../../UI/Button/Button";
import Input from "../../UI/Input/Input";
import { updateObject, checkValidity } from "../../Utility/Utility";
import axios from "axios";
import SinglePost from "./SinglePost/SinglePost";
import "./Post.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  withRouter,
  Redirect
} from "react-router-dom";
import AuthContext from "../../context/auth-context";

class MyPost extends Component {
  state = {
    posts: []
  };

  componentDidMount() {
    console.log(this.props);
    axios
      .get("http://localhost:3300/mypost")
      .then(response => {
        console.log(response.data);
        let posts = JSON.parse(response.data.posts);
        console.log(posts);
        const updatedPosts = posts.map(post => {
          return {
            ...post
          };
        });
        this.setState({ posts: updatedPosts });
        console.log(this.state.posts);
        // console.log( response );
      })
      .catch(error => {
        console.log(error);
        // this.setState({error: true});
      });
  }

  render() {
    let content = null;
    //console.log(this.props.user);

    content = <p style={{ textAlign: "center" }}>Something went wrong!</p>;

    let posts = <h2 style={{ textAlign: "center" }}>No Posts !!!</h2>;
    //if (!this.state.error) {
    posts = this.state.posts.map(post => {
      console.log(post.id);
      return (
        <SinglePost
          key={post.id}
          userId={post.user_id}
          id={post.id}
          user_Id={post.user_id}
          title={post.post}
          props={this.props}
          //   clicked={() => this.postSelectedHandler(post.id)}
        />
      );
    });
    // }

    content = (
      <div>
        <AuthContext.Consumer>
          {context => {
            console.log(context);
            return (
              <Fragment>
                <hr />
                <section className="Posts">{posts}</section>
                <hr />
                <Link to="/">Home Page</Link>|
                <Link to="/CreatePost">Create Post</Link>|
                <Link to="/SignIn">Show All Post</Link>|
              </Fragment>
            );
          }}
        </AuthContext.Consumer>
      </div>
    );

    return (
      <div>
        {/* <h1>{this.props.user.name}</h1>
        <Button clicked={this.props.logoutUserHandler}>Signout</Button>
        <hr /> */}

        {content}
      </div>
    );
  }
}

export default withRouter(MyPost);
