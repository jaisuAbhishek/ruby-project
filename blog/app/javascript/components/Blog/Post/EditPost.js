import React, { Component, Fragment } from "react";
import Button from "../../UI/Button/Button";
import Input from "../../UI/Input/Input";
import { updateObject, checkValidity } from "../../Utility/Utility";
import axios from "axios";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  withRouter,
  Redirect
} from "react-router-dom";
import AuthContext from "../../context/auth-context";

class CreatePost extends Component {
  state = {
    postForm: {
      post: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Enter Your Post"
        },
        value: this.props.location.state.postData || "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      }
    },
    isAuthenticated: true,
    requestType: 1
  };

  postCreateHandler = () => {
    event.preventDefault();

    const formData = {};
    for (let formElementIdentifier in this.state.postForm) {
      formData[formElementIdentifier] = this.state.postForm[
        formElementIdentifier
      ].value;
    }
    // console.log(formData);

    axios
      .put(
        "http://localhost:3300/posts/" + this.props.location.state.id,
        formData
      )
      .then(response => {
        //dispatch(setPosts(response.data));
        console.log(response);
        this.props.history.push("/SignIn");
        //    this.setState({posts:response.data});
        //console.log(this.state.posts);
      })
      .catch(error => {
        console.log(error);
      });
  };

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedFormElement = updateObject(
      this.state.postForm[inputIdentifier],
      {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          this.state.postForm[inputIdentifier].validation
        ),
        touched: true
      }
    );
    const updatedPostForm = updateObject(this.state.postForm, {
      [inputIdentifier]: updatedFormElement
    });

    let formIsValid = true;
    for (let inputIdentifier in updatedPostForm) {
      formIsValid = updatedPostForm[inputIdentifier].valid && formIsValid;
    }
    this.setState({ postForm: updatedPostForm, formIsValid: formIsValid });
  };

  render() {
    console.log(this.props.location.state);
    let content = null;
    // console.log(this.props.user);
    const formElementsArray = [];
    for (let key in this.state.postForm) {
      formElementsArray.push({
        id: key,
        config: this.state.postForm[key]
      });
    }

    content = (
      <div>
        <AuthContext.Consumer>
          {context => {
            //console.log(context);
            return (
              <Fragment>
                <hr />
                <form onSubmit={this.postCreateHandler}>
                  {formElementsArray.map(formElement => (
                    <Input
                      key={formElement.id}
                      elementType={formElement.config.elementType}
                      elementConfig={formElement.config.elementConfig}
                      value={formElement.config.value}
                      invalid={!formElement.config.valid}
                      shouldValidate={formElement.config.validation}
                      touched={formElement.config.touched}
                      changed={event =>
                        this.inputChangedHandler(event, formElement.id)
                      }
                    />
                  ))}
                  <Button btnType="Success" disabled={!this.state.formIsValid}>
                    Create New Post
                  </Button>
                </form>
                <hr />
                <header></header>
                <Link to="/">Home Page</Link>
              </Fragment>
            );
          }}
        </AuthContext.Consumer>
      </div>
    );
    return <div>{content}</div>;
  }
}

export default withRouter(CreatePost);
