import React, { Component, Fragment } from "react";
import Button from "../../UI/Button/Button";
import Input from "../../UI/Input/Input";
import { updateObject, checkValidity } from "../../Utility/Utility";
import axios from "axios";
import SinglePost from "./SinglePost/SinglePost";
import "./Post.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  withRouter,
  Redirect
} from "react-router-dom";
import AuthContext from "../../context/auth-context";

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: []
    };
  }

  // shouldComponentUpdate = () => {
  //   console.log("Inside Derived State");
  //   axios
  //     .get("http://localhost:3300/showall")
  //     .then(response => {
  //       console.log(response.data);
  //       let posts = JSON.parse(response.data.posts);
  //       console.log(posts);
  //       const updatedPosts = posts.map(post => {
  //         return {
  //           ...post
  //         };
  //       });
  //       this.setState({ posts: updatedPosts });
  //       console.log(this.state.posts);
  //       return true;
  //     })
  //     .catch(error => {
  //       console.log(error);
  //       return true;
  //     });
  // };

  componentDidMount() {
    console.log(
      "oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
    );
    axios
      .get("http://localhost:3300/showall")
      .then(response => {
        console.log(response.data);
        let posts = JSON.parse(response.data.posts);
        console.log(posts);
        const updatedPosts = posts.map(post => {
          return {
            ...post
          };
        });
        this.setState({ posts: updatedPosts });
        console.log(this.state.posts);
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleButtonClick = () => {
    this.componentDidMount();
  };

  render() {
    let content = null;

    content = <p style={{ textAlign: "center" }}>Something went wrong!</p>;

    let posts = <h2 style={{ textAlign: "center" }}>No Posts !!!</h2>;

    posts = this.state.posts.map(post => {
      console.log(post.id);
      return (
        <SinglePost
          key={post.id}
          userId={this.context.user.id}
          id={post.id}
          user_Id={post.user_id}
          title={post.post}
          props={this.props}
          post={post}
          clicked={this.handleButtonClick}
        />
      );
    });

    content = (
      <div>
        <AuthContext.Consumer>
          {context => {
            console.log(context);
            return (
              <Fragment>
                <hr />
                <section className="Posts">{posts}</section>
                <hr />
                <header>
                  <Button clicked={this.props.deleteUserHandler}>
                    Delete User
                  </Button>
                </header>
                <header></header>
                <Link to={"/EditUser/" + this.context.user.id}>Edit User</Link>|
                <Link to="/">Home Page</Link>|
                <Link to={this.context.user.id + "/CreatePost"}>
                  Create Post
                  <div></div>
                </Link>
                |<Link to={this.context.user.id + "/MyPost"}>Show My Post</Link>
                |
              </Fragment>
            );
          }}
        </AuthContext.Consumer>
      </div>
    );

    return (
      <div>
        <h1>{this.context.user.name}</h1>
        <Button clicked={this.props.logoutUserHandler}>Signout</Button>
        <hr />
        {content}
      </div>
    );
  }
}
Post.contextType = AuthContext;
export default withRouter(Post);
