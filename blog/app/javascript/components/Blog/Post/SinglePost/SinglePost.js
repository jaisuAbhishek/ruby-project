import React, { Component, Fragment } from "react";

import "./SinglePost.css";
import { render } from "react-dom";
import Button from "../../../UI/Button/Button";
import axios from "axios";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  withRouter,
  Redirect
} from "react-router-dom";
import authContext from "../../../context/auth-context";

class SinglePost extends Component {
  //console.log("Inside Single Post");
  //console.log(props.id);
  constructor(props) {
    super(props);
  }
  state = {
    post: {}
  };

  componentDidMount = () => {
    axios
      .get("http://localhost:3300/posts/" + this.props.id)
      .then(response => {
        console.log(response.data.post);
        this.setState({ post: JSON.parse(response.data.post) });
      })
      .catch(error => console.log(error));
  };

  deletePostHandler = () => {
    axios
      .delete("/posts/" + this.props.id)
      .then(response => {
        //console.log("This is response redirect", this.props.history);
        //location.reload();
        this.props.clicked();
        //this.props.history.push("/");
      })
      .catch(error => console.log(error));
  };

  render() {
    let admin = null;
    // console.log(this.props.userId, "  ", this.props.user_Id);
    console.log(this.state.post);
    if (this.props.userId == this.props.user_Id) {
      admin = (
        <div>
          <authContext.Consumer>
            {context => {
              return (
                <Fragment>
                  <Button clicked={this.deletePostHandler}>Delete Post</Button>
                  <Link
                    to={{
                      pathname:
                        "/" +
                        this.context.user.id +
                        "/EditPost/" +
                        this.props.id,
                      state: {
                        id: this.props.id,
                        postData: this.state.post["post"]
                      }
                    }}
                  >
                    Edit Post
                  </Link>
                </Fragment>
              );
            }}
          </authContext.Consumer>
        </div>
      );
    }

    return (
      <article className="Post">
        <Link
          to={{
            pathname: this.context.user.id + "/ShowPost/" + this.props.id,
            state: { id: this.props.id }
          }}
        >
          <h1>{this.props.title}</h1>
        </Link>
        {admin}
        {/* <div className="Info">
      <div className="Author">{props.author}</div>
    </div> */}
      </article>
    );
  }
}
SinglePost.contextType = authContext;
export default withRouter(SinglePost);
