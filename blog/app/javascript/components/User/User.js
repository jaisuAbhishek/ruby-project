import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  withRouter,
  Redirect
} from "react-router-dom";

import classes from "./User.css";
import { render } from "react-dom";
import axios from "axios";

import AuthContext from "../context/auth-context";
import Post from "../Blog/Post/Post";

class User extends Component {
  constructor() {
    super();
    this.state = {
      isAuthenticated: false
    };
  }

  //   componentDidMount = () => {
  //     axios
  //       .get("http://localhost:3300/isauthenticated")
  //       .then(response => {
  //         console.log(response);
  //       })
  //       .catch(error => {
  //         console.log(error);
  //       });
  //   };

  render() {
    let content = null;
    if (!this.state.isAuthenticated) {
      content = (
        <div>
          <AuthContext.Consumer>
            {context => {
              //console.log(context);
              return (
                <header>
                  <nav>
                    <ul>
                      <li>
                        <Link to="/SignIn">SignIn</Link>
                      </li>
                      <li>
                        <Link to="/SignUp">SignUp</Link>
                      </li>
                    </ul>
                  </nav>
                </header>
              );
            }}
          </AuthContext.Consumer>
        </div>
      );
    } else {
      content = (
        <AuthContext.Consumer>
          {context => {
            return <Post />;
          }}
        </AuthContext.Consumer>
      );
    }
    return <div>{content}</div>;
  }
}

User.contextType = AuthContext;

export default withRouter(User);
