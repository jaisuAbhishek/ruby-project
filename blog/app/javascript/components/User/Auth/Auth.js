import React, { Component, Fragment } from "react";

import axios from "axios";

import Button from "../../UI/Button/Button";
import Spinner from "../../UI/Spinner/Spinner";
import classes from "./Auth.css";
//import axios from '../../../axios-orders';
import Input from "../../UI/Input/Input";
//import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'
import { updateObject, checkValidity } from "../../Utility/Utility";
import SignUp from "../Create/Create";
import Post from "../../Blog/Post/Post";

import AuthContext from "../../context/auth-context";
import ContextProvider from "../../context/ContextProvider";
import authContext from "../../context/auth-context";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  withRouter,
  Redirect
} from "react-router-dom";

class Auth extends Component {
  state = {
    userForm: {
      // mobile_no:{
      //     elementType: 'input',
      //     elementConfig: {
      //         type: 'text',
      //         placeholder: 'Your Mobile No.'
      //     },
      //     value: '',
      //     validation: {
      //         required: true,
      //         minLength:10,
      //         maxLength:10,
      //         isNumeric: true
      //     },
      //     valid: false,
      //     touched: false
      // },
      email: {
        elementType: "input",
        elementConfig: {
          type: "email",
          placeholder: "Mail Address"
        },
        value: "",
        validation: {
          required: true,
          isEmail: true
        },
        valid: false,
        touched: false
      },
      password: {
        elementType: "input",
        elementConfig: {
          type: "password",
          placeholder: "Password"
        },
        value: "",
        validation: {
          required: true,
          minLength: 6,
          maxLength: 40
        },
        valid: false,
        touched: false
      }
    },
    userId: 0,
    user: [],
    formIsValid: false,
    loading: false
  };

  componentDidMount = () => {
    this.setState({ loading: true });
    axios
      .get("http://localhost:3300/isauthenticated")
      .then(response => {
        console.log(response.data.isauthenticated);
        this.setState({ userId: response.data.isauthenticated });
        console.log(response.data.user);
        this.context.loginHandler(true, JSON.parse(response.data.user));
        // console.log(this.state);
        this.setState({ loading: false });
      })
      .catch(error => {
        this.setState({ loading: false });
        console.log(error);
      });
  };

  deleteUserHandler = () => {
    axios
      .delete("http://localhost:3300/deleteusers")
      .then(response => {
        this.props.history.push("/");
      })
      .catch(error => {
        console.log(error);
      });
  };

  logoutUserHandler = () => {
    axios
      .get("http://localhost:3300/logout")
      .then(response => {
        this.context.loginHandler(false, []);
        this.props.history.push("/");
      })
      .catch(error => {
        console.log(error);
      });
  };

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedFormElement = updateObject(
      this.state.userForm[inputIdentifier],
      {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          this.state.userForm[inputIdentifier].validation
        ),
        touched: true
      }
    );
    const updatedUserForm = updateObject(this.state.userForm, {
      [inputIdentifier]: updatedFormElement
    });

    let formIsValid = true;
    for (let inputIdentifier in updatedUserForm) {
      formIsValid = updatedUserForm[inputIdentifier].valid && formIsValid;
    }
    this.setState({ userForm: updatedUserForm, formIsValid: formIsValid });
  };

  userHandler = event => {
    event.preventDefault();
    //console.log(this.context);
    //console.log(Authentication);
    const formData = {};
    for (let formElementIdentifier in this.state.userForm) {
      formData[formElementIdentifier] = this.state.userForm[
        formElementIdentifier
      ].value;
    }

    axios
      .post("http://localhost:3300/login", formData)
      .then(response => {
        //console.log(response.data.user);
        this.context.loginHandler(true, JSON.parse(response.data.user));

        this.setState({
          userId: response.data.session.user_id,
          user: JSON.parse(response.data.user)
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    // console.log(this.state);
    // if (this.state.isSession == "true") {
    //   console.log("eeeeeeeeeee");
    // } else {
    //   console.log("hhhhhhhhhhhhhhhhhhhhh");
    // }
    //console.log(this.state.user);
    let form = null;
    const formElementsArray = [];
    for (let key in this.state.userForm) {
      formElementsArray.push({
        id: key,
        config: this.state.userForm[key]
      });
    }
    if (this.state.loading) {
      form = <Spinner />;
    }
    if (!this.state.loading && this.state.userId == 0) {
      form = (
        <authContext.Consumer>
          {context => {
            // console.log(context);
            return (
              <Fragment>
                <form onSubmit={this.userHandler}>
                  {formElementsArray.map(formElement => (
                    <Input
                      key={formElement.id}
                      elementType={formElement.config.elementType}
                      elementConfig={formElement.config.elementConfig}
                      value={formElement.config.value}
                      invalid={!formElement.config.valid}
                      shouldValidate={formElement.config.validation}
                      touched={formElement.config.touched}
                      changed={event =>
                        this.inputChangedHandler(event, formElement.id)
                      }
                    />
                  ))}
                  <Button btnType="Success" disabled={!this.state.formIsValid}>
                    SignIn
                  </Button>
                </form>
              </Fragment>
            );
          }}
        </authContext.Consumer>
      );
    }

    if (
      !this.state.loading &&
      (this.context.authenticated || this.state.userId != 0)
    ) {
      //console.log("After Login Part");
      form = (
        <authContext.Consumer>
          {context => {
            return (
              <Fragment>
                <Post
                  user={this.state.user}
                  deleteUserHandler={this.deleteUserHandler}
                  logoutUserHandler={this.logoutUserHandler}
                />
              </Fragment>
            );
          }}
        </authContext.Consumer>
      );
    }

    return (
      <div className={classes.UserData}>
        {
          //   <authContext.Consumer>
          //     {context => {
          //       console.log(context);
          //       return form;
          //     }}
          //   </authContext.Consumer>
        }

        {form}
      </div>
    );
  }
}

Auth.contextType = authContext;

export default withRouter(Auth);
