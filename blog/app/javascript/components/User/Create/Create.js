import React, {Component} from 'react'

import axios from 'axios';

import Button from '../../UI/Button/Button';
import Spinner from '../../UI/Spinner/Spinner';
import classes from './Create.css';
//import axios from '../../../axios-orders';
import Input from '../../UI/Input/Input';
//import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'
import { updateObject, checkValidity } from '../../Utility/Utility';


class SignIn extends Component{
    state = {
        userForm:{
            //id:0,
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            mobile_no:{
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Mobile No.'
                },
                value: '',
                validation: {
                    required: true,
                    minLength:10,
                    maxLength:10,
                    isNumeric: true
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6,
                    maxLength:40
                },
                valid: false,
                touched: false
            },
            // confirm_password: {
            //     elementType: 'input',
            //     elementConfig: {
            //         type: 'password',
            //         placeholder: 'Confirm Password'
            //     },
            //     value: '',
            //     validation: {
            //         required: true,
            //         minLength: 6
            //     },
            //     valid: false,
            //     touched: false
            // }
            
            //created_at:null,
            //updated_at:null
        },
        isSignUp:true,
        formIsValid: false
    };

    //showScreen = null;

    //create = null;


    inputChangedHandler = (event, inputIdentifier) => {
        
        const updatedFormElement = updateObject(this.state.userForm[inputIdentifier], {
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.userForm[inputIdentifier].validation),
            touched: true,
            
        });
        const updatedUserForm = updateObject(this.state.userForm, {
            [inputIdentifier]: updatedFormElement
        });
        
        let formIsValid = true;
        for (let inputIdentifier in updatedUserForm) {
            formIsValid = updatedUserForm[inputIdentifier].valid && formIsValid;
           // if (updatedUserForm[inputIdentifier].valid==false){
            //   event.target.classList.add('Invalid')
            //}
        }
        this.setState({userForm: updatedUserForm, formIsValid: formIsValid});
    }


    userHandler = ( event ) => {
        event.preventDefault();
  
        const formData = {};
        for (let formElementIdentifier in this.state.userForm) {
            formData[formElementIdentifier] = this.state.userForm[formElementIdentifier].value;
        }
        console.log(formData);
        axios.post( 'http://localhost:3300/users' ,formData)
            .then( response => {
               //dispatch(setPosts(response.data));
               console.log(response);
               this.props.history.push("/");
            //    this.setState({posts:response.data});
               //console.log(this.state.posts);
            } )
            .catch( error => {
                console.log(error);
            } );

        // const order = {
        //     ingredients: this.props.ings,
        //     price: this.props.price,
        //     orderData: formData,
        //     userId: this.props.userId
        // }

        //this.props.onOrderBurger(order, this.props.token);
        
    }

    render(){
        const formElementsArray = [];
        for (let key in this.state.userForm) {
            formElementsArray.push({
                id: key,
                config: this.state.userForm[key]
            });
        }
        let form = (
            <form onSubmit={this.userHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <Button btnType="Success" disabled={!this.state.formIsValid}>SignUp</Button>
            </form>
        );
        if ( this.props.loading ) {
            form = <Spinner />;
        }
        return (
            <div className={classes.UserData}>
                <h4>Enter your Details</h4>
                {form}
            </div>
        );
    }
}

export default SignIn