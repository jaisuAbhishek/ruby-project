import React, { Component, Fragment } from "react";
import { render } from "react-dom";
import axios from "axios";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  withRouter,
  Redirect
} from "react-router-dom";
import authContext from "../context/auth-context";

class UserDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: []
    };
  }

  componentDidMount() {
    console.log(
      "this.props.match.params.id------------------------------------"
    );
    axios
      .get(
        "http://localhost:3300/userdetails/" + this.props.match.params.userId
      )
      .then(response => {
        console.log(response.data);
        let userr = JSON.parse(response.data.user);

        this.setState({ user: userr });

        console.log(this.state.user);
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    console.log(this.context.user);
    return (
      <div>
        <authContext.Consumer>
          {context => {
            return (
              <Fragment>
                <h2>{this.state.user.id}</h2>
                <h2>{this.state.user.name}</h2>
                <h2>{this.state.user.email}</h2>
                <h2>{this.state.user.mobile_no}</h2>
                <Link to="/">Home Page</Link>|
                <Link to={"/" + this.context.user.id + "/CreatePost"}>
                  Create Post
                  <div></div>
                </Link>
                |
                <Link to={"/" + this.context.user.id + "/MyPost"}>
                  Show My Post
                </Link>
                |
              </Fragment>
            );
          }}
        </authContext.Consumer>
      </div>
    );
  }
}
UserDetails.contextType = authContext;
export default UserDetails;
