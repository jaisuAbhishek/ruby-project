import React, { Component, Fragment } from "react";

import axios from "axios";

import Button from "../../UI/Button/Button";
import Spinner from "../../UI/Spinner/Spinner";
import classes from "../Create/Create.css";
//import axios from '../../../axios-orders';
import Input from "../../UI/Input/Input";
//import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'
import { updateObject, checkValidity } from "../../Utility/Utility";
import AuthContext from "../../context/auth-context";
import authContext from "../../context/auth-context";

class Edit extends Component {
  state = {
    userForm: {
      //id:0,
      name: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Your Name"
        },
        value:
          typeof this.context.user != "undefined" ? this.context.user.name : "",
        validation: {
          required: true
        },
        valid: true,
        touched: true
      },
      mobile_no: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Your Mobile No."
        },
        value:
          typeof this.context.user != "undefined"
            ? this.context.user.mobile_no
            : "",
        validation: {
          required: true,
          minLength: 10,
          maxLength: 10,
          isNumeric: true
        },
        valid: true,
        touched: true
      },
      email: {
        elementType: "input",
        elementConfig: {
          type: "email",
          placeholder: "Mail Address"
        },
        value:
          typeof this.context.user != "undefined"
            ? this.context.user.email
            : "",
        validation: {
          required: true,
          isEmail: true
        },
        valid: true,
        touched: true
      },
      password: {
        elementType: "input",
        elementConfig: {
          type: "password",
          placeholder: "Password"
        },
        value: "",
        validation: {
          required: true,
          minLength: 6,
          maxLength: 40
        },
        valid: false,
        touched: false
      }
    },
    isSignUp: true,
    formIsValid: false
    //userValues: this.context.user
  };

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedFormElement = updateObject(
      this.state.userForm[inputIdentifier],
      {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          this.state.userForm[inputIdentifier].validation
        ),
        touched: true
      }
    );
    const updatedUserForm = updateObject(this.state.userForm, {
      [inputIdentifier]: updatedFormElement
    });

    let formIsValid = true;
    for (let inputIdentifier in updatedUserForm) {
      formIsValid = updatedUserForm[inputIdentifier].valid && formIsValid;
      // if (updatedUserForm[inputIdentifier].valid==false){
      //   event.target.classList.add('Invalid')
      //}
    }
    this.setState({ userForm: updatedUserForm, formIsValid: formIsValid });
  };

  userHandler = event => {
    event.preventDefault();

    const formData = {};
    for (let formElementIdentifier in this.state.userForm) {
      formData[formElementIdentifier] = this.state.userForm[
        formElementIdentifier
      ].value;
    }
    console.log(formData);
    axios
      .put("http://localhost:3300/edituser", formData)
      .then(response => {
        //dispatch(setPosts(response.data));
        console.log(response);
        this.props.history.push("/");
        //    this.setState({posts:response.data});
        //console.log(this.state.posts);
      })
      .catch(error => {
        console.log(error);
      });

    // const order = {
    //     ingredients: this.props.ings,
    //     price: this.props.price,
    //     orderData: formData,
    //     userId: this.props.userId
    // }

    //this.props.onOrderBurger(order, this.props.token);
  };

  render() {
    const formElementsArray = [];
    for (let key in this.state.userForm) {
      formElementsArray.push({
        id: key,
        config: this.state.userForm[key]
      });
    }
    // userVal = ["name", "mobile_no", "email", "password"];
    console.log(this.context.user);
    // let i = 0;
    let form = (
      <AuthContext.Consumer>
        {context => {
          // console.log(this.conntext);
          console.log(formElementsArray);

          return (
            <Fragment>
              <form onSubmit={this.userHandler}>
                {formElementsArray.map(formElement => (
                  <Input
                    key={formElement.id}
                    elementType={formElement.config.elementType}
                    elementConfig={formElement.config.elementConfig}
                    value={formElement.config.value}
                    invalid={!formElement.config.valid}
                    shouldValidate={formElement.config.validation}
                    touched={formElement.config.touched}
                    changed={event =>
                      this.inputChangedHandler(event, formElement.id)
                    }
                  />
                ))}
                <Button btnType="Success" disabled={!this.state.formIsValid}>
                  Submit Changes
                </Button>
              </form>
            </Fragment>
          );
        }}
      </AuthContext.Consumer>
    );
    if (this.props.loading) {
      form = <Spinner />;
    }
    return (
      <div className={classes.UserData}>
        {/* <AuthContext.Consumer>
          {context => {
            console.log(context);
          }}
        </AuthContext.Consumer> */}
        <h4>Edit User</h4>
        {form}
      </div>
    );
  }
}
Edit.contextType = authContext;
export default Edit;
