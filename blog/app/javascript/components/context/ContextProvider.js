import React, { Component } from "react";
import { render } from "react-dom";
import authContext from "./auth-context";
import axios from "axios";

class ContextProvider extends Component {
  constructor() {
    super();
    this.state = {
      authenticated: false
    };
  }

  render() {
    return (
      <authContext.Provider
        value={{
          authenticated: this.state.authenticated,
          user: this.state.user,
          loginHandler: (value, userData) => {
            //console.log(userData);

            this.setState({ authenticated: value, user: userData });
            console.log(this.state.user);
          }
        }}
      >
        {this.props.children}
      </authContext.Provider>
    );
  }
}

export default ContextProvider;
