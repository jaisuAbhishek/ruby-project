import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  Redirect
} from "react-router-dom";

import Blog from "./Blog/Blog";
import UserDetails from "./User/UserDetails";
import SignIn from "./User/Auth/Auth";
import SignUp from "./User/Create/Create";
import Edit from "./User/Edit/Edit";
import AuthContext from "./context/auth-context";
import ContextProvider from "./context/ContextProvider";
import CreatePost from "./Blog/Post/CreatePost";
import EditPost from "./Blog/Post/EditPost";
import MyPost from "./Blog/Post/MyPost";
import FullPost from "./Blog/Post/FullPost/FullPost";

class Main extends React.Component {
  render() {
    return (
      <ContextProvider>
        <Router>
          <div></div>
          <Switch>
            <Route path="/" exact component={Blog} />
            <Route path="/SignIn" exact component={SignIn} />
            <Route path="/SignUp" exact component={SignUp} />
            <Route path="/EditUser/:userId" exact component={Edit} />
            <Route path="/User/:userId" exact component={UserDetails} />
            <Route path="/:userId/CreatePost" exact component={CreatePost} />
            <Route path="/:userId/EditPost/:id" exact component={EditPost} />
            <Route path="/:userId/MyPost" exact component={MyPost} />
            <Route path="/:userId/ShowPost/:id" exact component={FullPost} />
          </Switch>
        </Router>
      </ContextProvider>
    );
  }
}

export default Main;
