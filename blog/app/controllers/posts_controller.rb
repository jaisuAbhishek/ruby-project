class PostsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_cache_headers
  before_action :create_user
  # def index
  #   @user=User.new
  #   uid=session[:user_id]
  #   #p "UID #{uid}"
  #   if uid.blank?
  #     redirect_to login_path
  #   else
  #     @user=User.find(uid)
  #   end
  # end

  def show_all
    if session[:user_id].present?
      @posts = Post.all
      render json: {
          #session: session,
          posts: @posts.to_json
        },status: 200
    # else
    #   render json: {
    #       #session: session,
    #       message: "Session not found"
    #     },status: 404
    end
  end

  def mypost
    #begin
      #@post = Post.where(user_id: @user.id)
      
      @post=@user.posts
      render json: {
        #session: session,
        posts: @post.to_json
      },status: 200
    #rescue
      # render json: {
      #   #session: session,
      #   posts: @post.to_json
      # },status: 404
    #end
  end
  def show
    begin
      @post = Post.find(params[:id])
      @usersh = User.find(@post.user_id)
      render json: {
        post: @post.to_json,
        user: @usersh.to_json
      },status: 200
    rescue
      render json: {
        
      },status: 404
    end
  end

  # def new
  #   @post = Post.new
  #   #create_user
  # end
  
  def create
    if @check
      begin
        @post = @user.posts.create(post: params[:post])
        if @post.save!
          render json: {
          id: @post.id.to_json
        },status: 200
        # else
        #   render json: {
        #   status: 422
        # },status: 422
        end
      rescue => exception
        render json: {
          status: 422
        },status: 422
      end
      
    end
  end

  def destroy
    begin
      @post = Post.find_by(id: params[:id])
    if @post.delete && @check
      render json: {
        #session: session,
        
      },status: 200
    # else
    #   p "Inside else"
    #    render json: {
    #     #session: session,
        
    #   },status: 422
    end
    rescue => exception
      render json: {
        message: exception,
        
      },status: 422
    end
    
  end

  # def edit
  #   @post = Post.find(params[:id])
  #   if @post.user_id!=@user.id
  #     redirect_to showall_path
  #   end
  #   #@post.update(name: params[:post][:name], description: params[:post][:comment])
  #   #redirect_to '/posts'
  # end

  def update
    begin
      @post = Post.find(params[:id])
      if @post.user_id==@user.id
        @post.update!(post: params[:post])
        render json: {
          #session: session,
          
        },status: 200
      end
    rescue => exception
      render json: {
        #session: session,
        
      },status: 422
    end
    
  end

  private

  # def post_params
  #   params.require(:post).permit(:post)
  # end

  def create_user
    uid=session[:user_id]
    @check = false;
    if uid.present?
      @user = User.find(uid)
      @check = true;
    else
      render json: {
          status: 422
        },status: 422
    end
  end

  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

end
