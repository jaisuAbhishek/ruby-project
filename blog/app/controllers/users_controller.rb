class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_cache_headers
  
  @@isAuthenticated=0
  

  # def index
    
  # end

  def user_details
    begin
      @user=User.find(params[:id]);
      if @user.present?
      render json: {
            user:@user.to_json
          },status: 200
      # else
      #   render json: {
      #       user:"User not found"
      #     },status: 404
      end
      rescue => exception
        render json: {
            user:"User not found"
          },status: 404
    end
    
  end

  def get_login
   # p "isAuthenticated #{@@isAuthenticated}"
   begin
     if session[:user_id].present?
      @user=User.find(session[:user_id])
      @@isAuthenticated=@user.id
      #p "User Inside get_login #{@user}"
      render json: {
          isauthenticated: @@isAuthenticated.to_json,
          user:@user.to_json
        },status: 200
    else
      render json: {
          isauthenticated: @@isAuthenticated.to_json
         },status: 404
      end
    rescue => exception
     render json: {
            isauthenticated: @@isAuthenticated.to_json
          },status: 404
    end
    
  end
  def main
    render "main"
  end

  def create
    begin 
      @user = User.new(user_params)
      if @user.save!
        render json: {
          status: 200
        },status: 200
      # else
      #   render json: {
        
      #     },status: 422
      end
    rescue => e 
      #puts "======================="
      render json: {
        error: e.message
        },status: 422
      #puts e.message
    end
  end

  # def new
  #   @user = User.new
  # end

  # def edit
  #   uid=session[:user_id]
  #   if uid.present?
  #     @user = User.find(uid)
  #   else
  #     render json: {
        
  #     },status: 404
  #   end
  # end

  def update
    uid=session[:user_id]
    if uid.present?
      begin
        @user = User.find(uid)
        @user.update!(user_params)
        render json: {},status: 200
      rescue
        render json: {
        
        },status: 422

      end
    # else
    #   render json: {
        
    #   },status: 422
    end
  end

  def destroy
    uid=session[:user_id]
    if uid.present?
      @user = User.find(uid) 
      if @user.destroy
        #session[:user_id] = nil
        reset_session
        @@isAuthenticated = 0
        render json: {
          
        },status: 200
      end
    else
      render json: {
        
      },status: 404
    end
  end

  # def login
  #   if session[:user_id].present?
  #     redirect_to posts_path
  #   else
  #     @user=User.new
  #   end
  # end

  def verify
    #p "Inside Verify"

    @user=User
    .find_by(email: params[:email])
    .try(:authenticate, params[:password]) 
    #p "User is #{@user}"
    if @user.present?
      #p "User is present"
      session[:user_id] = @user.id
      @@isAuthenticated=@user.id
      render json: {
        session: session,
        user: @user.to_json
      },status: 200
    else
      #p "Inside else block"
      render json: {
        #status: 401
      },status: 401
    end
  end

  def logout
    reset_session
    @@isAuthenticated = 0
    render json: {
      status: 200
    },status: 200
  end

  private

  def user_params
    params.permit(:name, :email, :mobile_no, :password)
  end

  def set_cache_headers
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

end
