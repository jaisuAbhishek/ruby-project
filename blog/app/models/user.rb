class User < ApplicationRecord
    
    has_many :posts, dependent: :delete_all
    has_secure_password

    #EMAIL_REGEXP=/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/i
    validates_presence_of :email
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
    validates_uniqueness_of :email
    validates_presence_of :mobile_no
    validates_uniqueness_of :mobile_no
    validates_format_of :mobile_no, with: /[0-9]{10}/
    validates_presence_of :password_digest
    PASSWORD_FORMAT = /\A
        (?=.{8,})          # Must contain 8 or more characters
        (?=.*\d)           # Must contain a digit
        (?=.*[a-z])        # Must contain a lower case character
        (?=.*[A-Z])        # Must contain an upper case character
        (?=.*[[:^alnum:]]) # Must contain a symbol
    /x

    

    validates :password, 
        presence: true, 
        length: { :within => 6..40 }, 
        format: { with: PASSWORD_FORMAT }, 
        confirmation: true, 
        on: :create

    validates :password, 
        allow_nil: false, 
        length: { :within => 6..40 }, 
        format: { with: PASSWORD_FORMAT }, 
        confirmation: true, 
        on: :update

    private

    # def is_number? string
    #     true if Integer(string) rescue false
    #   end
end
