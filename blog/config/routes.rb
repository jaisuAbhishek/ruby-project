Rails.application.routes.draw do
  resources :posts

  # get 'posts/index'

  # get 'posts/show' => 'posts#show'

  # get 'posts/new'

  # get 'posts/create'

  # delete 'posts/delete' => 'posts#destroy'

  # get 'posts/update'

  # get '/posts/edit' => 'posts#edit'

  get '/posts/:id' => 'posts#show'

  post '/posts/create' => 'posts#create'

  get '/showall' => 'posts#show_all'

  get '/mypost' => 'posts#mypost'

  resources :users, except: [:show]
  #get 'users/create'
  #resources :posts, expect: [:show]
  #get 'users/index'

  #get 'users/create'

  #get 'users/new'

  #get 'users/update'

  #get 'users/edit'

  get '/isauthenticated' => 'users#get_login'

  get '/userdetails/:id' => 'users#user_details'

  put '/edituser' => 'users#update'

  delete '/deleteusers' => 'users#destroy'

  get 'login' => 'users#login'

  post 'login' => 'users#verify'

  get 'logout' => 'users#logout'

  get '*path'=>'users#main'

  root :to => "users#main"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
